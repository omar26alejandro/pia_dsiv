<?php

class habitaciones
{

    //función para obtener todos los habitaciones
    public static function all()
    {
		$db=Db::getConnect();
        $sql=$db->query('SELECT habitacion.IdHabitacion,
								habitacion.DescHabitacion,
								habitacion.Nivel,
								tipohabitacion.DescpTipoHabitacion,
								edificio.DescEdificio,
								vista.DescVista,
								usuario.Nombre,
								usuario.APaterno,
								usuario.AMaterno,
								estatus.DescEstatus
								FROM `habitacion`
						INNER JOIN tipohabitacion
						ON tipohabitacion.IdTipoHabitacion = habitacion.IdTipoHabitacion
						INNER JOIN edificio
						ON edificio.IdEdificio = habitacion.IdEdificio
						INNER JOIN vista
						ON vista.IdVista = habitacion.IdVista
						INNER JOIN usuario
						ON usuario.IdUsuario = habitacion.IdUsuario
						INNER JOIN estatus
						ON estatus.IdEstatus = habitacion.IdEstatus');
        $Habitaciones = $sql->fetchAll();
	
		return $Habitaciones;
	}

	// la función para buscar por el id
	public static function SelectId($id)
	{
		$db=Db::getConnect();
		$select=$db->prepare('SELECT habitacion.IdHabitacion,
									habitacion.DescHabitacion,
									habitacion.Nivel,
									habitacion.IdTipoHabitacion,
									habitacion.IdEdificio,
									habitacion.IdVista,
									habitacion.IdUsuario,
									habitacion.IdEstatus,
									tipohabitacion.DescpTipoHabitacion,
									edificio.DescEdificio,
									vista.DescVista,
									usuario.Nombre,
									usuario.APaterno,
									usuario.AMaterno,
									estatus.DescEstatus
									FROM `habitacion`
							INNER JOIN tipohabitacion
							ON tipohabitacion.IdTipoHabitacion = habitacion.IdTipoHabitacion
							INNER JOIN edificio
							ON edificio.IdEdificio = habitacion.IdEdificio
							INNER JOIN vista
							ON vista.IdVista = habitacion.IdVista
							INNER JOIN usuario
							ON usuario.IdUsuario = habitacion.IdUsuario
							INNER JOIN estatus
							ON estatus.IdEstatus = habitacion.IdEstatus
							WHERE habitacion.IdHabitacion = :IdHabitacion');

		$select->bindValue(':IdHabitacion',$id);
		$select->execute();

		$Habitacion=$select->fetch();

		return $Habitacion;

	}

	//la función para actualizar 
	public static function update($IdHab, $idEdif, $Desc, $Nivel, $TipHab, $Vista, $IdUsuario, $Estatus)
	{
		$db=Db::getConnect();
		$update=$db->prepare('	UPDATE habitacion 
								SET DescHabitacion=:DescHabitacion,
									Nivel=:Nivel,
									IdTipoHabitacion=:IdTipoHabitacion,
									IdEdificio=:IdEdificio,
									IdVista=:IdVista,
									IdUsuario=:IdUsuario,
									IdEstatus=:IdEstatus
								WHERE IdHabitacion=:IdHabitacion');
		$update->bindValue(':IdHabitacion',$IdHab);
		$update->bindValue(':DescHabitacion',$Desc);
		$update->bindValue(':Nivel',$Nivel);
		$update->bindValue(':IdTipoHabitacion',$TipHab);
		$update->bindValue(':IdEdificio',$idEdif);
		$update->bindValue(':IdVista',$Vista);
		$update->bindValue(':IdUsuario',$IdUsuario);
		$update->bindValue(':IdEstatus',$Estatus);
		$update->execute();	

	}
	
	// la función para eliminar por el id
	public static function delete($id)
	{
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE FROM habitacion WHERE IdHabitacion=:IdHabitacion');
		$delete->bindValue(':IdHabitacion',$id);
		
		$delete->execute();

		return;

	}

	// la funcion para crear
    public static function create($Desc, $Nivel, $TipHab, $idEdif, $Vista, $IdUsuario, $Estatus)
	{
		$db=Db::getConnect();
		$insert=$db->prepare('INSERT INTO habitacion VALUES(NULL,:DescHabitacion,:Nivel,:IdTipoHabitacion,:IdEdificio,:IdVista,:IdUsuario,:IdEstatus)');
		$insert->bindValue('DescHabitacion',$Desc);
		$insert->bindValue('Nivel',$Nivel);
		$insert->bindValue('IdTipoHabitacion',$TipHab);
		$insert->bindValue('IdEdificio',$idEdif);
		$insert->bindValue('IdVista',$Vista);
		$insert->bindValue('IdUsuario',$IdUsuario);
		$insert->bindValue('IdEstatus',$Estatus);
		$insert->execute();
		$create = $db->lastInsertId();
		
		return $create;
	}
	
	//la funcion para obtener los tipos de habitaciones
    public static function SelectTipoHabitacion()
    {
        $db=Db::getConnect();
        $sql=$db->query('SELECT * FROM tipohabitacion');
        $TipoHabitaciones = $sql->fetchAll();
	
		return $TipoHabitaciones;
	}
}
?>