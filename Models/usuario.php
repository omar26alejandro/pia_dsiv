<?php

class Usuario
{

	//la función para obtener un usuario por el id
	public static function getById($id,$pass)
	{
		//buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM usuario WHERE 	IdUsuario=:id AND Passw=:Passw');
		$select->bindValue(':id',$id);
		$select->bindValue(':Passw',$pass);
		$select->execute();

		//asignarlo al objeto usuario
		$usuario=$select->fetch();
		return $usuario;
	}

    //función para obtener todos los usuarios
    public static function all()
    {
        $db=Db::getConnect();
        $sql=$db->query('SELECT * FROM usuario');
        $Usuario = $sql->fetchAll();
	
		return $Usuario;
	}

	//Funcion para obtener los usuarios y su rol
	public static function allUsuarios()
	{
		$db=Db::getConnect();
        $sql=$db->query('SELECT usuario.IdUsuario,
								usuario.Nombre,
								usuario.APaterno,
								usuario.AMaterno,
								usuario.IdRol,
								rol.Descrip
						FROM `usuario`
						INNER JOIN rol
						ON rol.IdRol = usuario.IdRol');

        $Usuarios = $sql->fetchAll();
  
    	return $Usuarios;

	}

	// la función para buscar por el usuario con el rol
	public static function SelectId($id)
	{
	  $db=Db::getConnect();
	  $select=$db->prepare(' SELECT usuario.IdUsuario,
									usuario.Nombre,
									usuario.APaterno,
									usuario.AMaterno,
									usuario.IdRol,
									rol.Descrip
							FROM `usuario`
							INNER JOIN rol
							ON rol.IdRol = usuario.IdRol
							WHERE usuario.IdUsuario = :IdUsuario');
	  $select->bindValue(':IdUsuario',$id);
	  $select->execute();
  
	  $Usuario=$select->fetch();
  
	  return $Usuario;
  
	}

	  //la función para actualizar 
	  public static function update($IdUsuario, $Nombre, $APaterno, $AMaterno, $IdRol)
	  {
		$db=Db::getConnect();
		$update=$db->prepare('  UPDATE usuario 
								SET IdUsuario=:IdUsuario,
								Nombre=:Nombre,
								APaterno=:APaterno,
								AMaterno=:AMaterno,
								IdRol=:IdRol
								WHERE IdUsuario=:IdUsuario');
		$update->bindValue(':IdUsuario',$IdUsuario);
		$update->bindValue(':Nombre',$Nombre);
		$update->bindValue(':APaterno',$APaterno);
		$update->bindValue(':AMaterno',$AMaterno);
		$update->bindValue(':IdRol',$IdRol);
		$update->execute(); 
	
	  }

	    // la funcion para crear
		public static function create($Nombre,$APaterno, $AMaterno,$IdRol)
		{
		  $db=Db::getConnect();
		  $insert=$db->prepare('INSERT INTO usuario VALUES(NULL,:Nombre,:APaterno,:AMaterno,:IdRol)');
		  $insert->bindValue(':Nombre',$Nombre);
		  $insert->bindValue(':APaterno',$APaterno);
		  $insert->bindValue(':AMaterno',$AMaterno);
		  $insert->bindValue(':IdRol',$IdRol);
		  $insert->execute();
		  
		  return $create;
		}
	  
	

	    // la función para eliminar por el id
		public static function delete($id)
		{
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE FROM usuario WHERE IdUsuario=:IdUsuario');
		$delete->bindValue(':IdUsuario',$id);

		$delete->execute();

		return;

		}
}
?>