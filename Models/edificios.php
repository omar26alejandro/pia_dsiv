<?php

class edificios
{

    //función para obtener todos los Edificios
    public static function all()
    {
        $db=Db::getConnect();
        $sql=$db->query('SELECT * FROM edificio');
        $Edificos = $sql->fetchAll();
	
		return $Edificos;
	}

	// la función para obtener datos por el id
	public static function SelectId($id)
	{
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM edificio WHERE IdEdificio =:IdEdificio');
		$select->bindValue(':IdEdificio',$id);
		$select->execute();

		$Edifico=$select->fetch();

		return $Edifico;

	}

	//la función para actualizar 
	public static function update($IdEdifico, $Descripcion)
	{
		$db=Db::getConnect();
		$update=$db->prepare('UPDATE edificio SET DescEdificio=:DescEdificio WHERE IdEdificio=:IdEdificio');
		$update->bindValue(':IdEdificio',$IdEdifico);
		$update->bindValue(':DescEdificio',$Descripcion);
		$update->execute();

		return;
	}
	
	// la función para eliminar por el id
	public static function delete($id)
	{
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE FROM edificio WHERE IdEdificio=:IdEdificio');
		$delete->bindValue(':IdEdificio',$id);
		
		$delete->execute();

		return;
	}
	
	// la función para crear
	public static function create($Desc)
	{
		$db=Db::getConnect();
		$insert=$db->prepare('INSERT INTO edificio VALUES(NULL,:DescEdificio)');
		$insert->bindValue('DescEdificio',$Desc);
		$insert->execute();
		$create = $db->lastInsertId();
		
		return $create;
	}
}
?>