<?php

class reservaciones
{

    //función para obtener todos los reservaciones
    public static function all()
    {
		$db=Db::getConnect();
        $sql=$db->query('SELECT reservacion.IdReservacion,
                        usuario.Nombre  as "Unombre",
                        usuario.APaterno  as "Upaterno",
                        usuario.AMaterno  as "Umaterno",
                        cliente.Nombre as "Cnombre",
                        cliente.APaterno  as "Cpaterno",
                        cliente.AMaterno  as "Cmaterno",
                        habitacion.DescHabitacion,
                        reservacion.FecInicio,
                        reservacion.FecFin,
                        reservacion.Monto,
                        reservacion.FecReservacion,
                        estatusreservacion.DescripEstatusReser
                        FROM `reservacion`
                        INNER JOIN usuario
                        ON usuario.IdUsuario = reservacion.IdUsuario
                        INNER JOIN cliente
                        ON cliente.IdCliente = reservacion.IdCliente
                        INNER JOIN habitacion
                        ON habitacion.IdHabitacion = reservacion.IdHabitacion
                        INNER JOIN estatusreservacion
                        ON estatusreservacion.IdEstatusReservacion = reservacion.IdEstatusReservacion');

        $Reservaciones = $sql->fetchAll();
	
		return $Reservaciones;
	}

	// la función para buscar por el id
	public static function SelectId($id)
	{
		$db=Db::getConnect();
        $select=$db->prepare('SELECT reservacion.IdReservacion,
                            reservacion.IdUsuario,
                            reservacion.IdCliente,
                            reservacion.IdHabitacion,
                            reservacion.IdEstatusReservacion,
                            usuario.Nombre  as "Unombre",
                            usuario.APaterno  as "Upaterno",
                            usuario.AMaterno  as "Umaterno",
                            cliente.Nombre as "Cnombre",
                            cliente.APaterno  as "Cpaterno",
                            cliente.AMaterno  as "Cmaterno",
                            habitacion.DescHabitacion,
                            reservacion.FecInicio,
                            reservacion.FecFin,
                            reservacion.Monto,
                            reservacion.FecReservacion,
                            estatusreservacion.DescripEstatusReser
                        FROM `reservacion`
                        INNER JOIN usuario
                        ON usuario.IdUsuario = reservacion.IdUsuario
                        INNER JOIN cliente
                        ON cliente.IdCliente = reservacion.IdCliente
                        INNER JOIN habitacion
                        ON habitacion.IdHabitacion = reservacion.IdHabitacion
                        INNER JOIN estatusreservacion
                        ON estatusreservacion.IdEstatusReservacion = reservacion.IdEstatusReservacion
                        WHERE IdReservacion=:IdReservacion');

		$select->bindValue(':IdReservacion',$id);
		$select->execute();

		$Reservacion=$select->fetch();

		return $Reservacion;

	}

    public static function SelectTipoEstatus()
    {
        $db=Db::getConnect();
        $sql=$db->query('SELECT * FROM estatusreservacion');
        $Estatus = $sql->fetchAll();
	
		return $Estatus;
    }
    
    public static function SelectEstus()
    {
        $db=Db::getConnect();
        $sql=$db->query('SELECT * FROM estatusreservacion WHERE IdEstatusReservacion="1" OR IdEstatusReservacion="2"');
        $Estatus = $sql->fetchAll();
	
		return $Estatus;
    }

	//la función para actualizar 
	public static function update($IdRes, $IdUsr, $IdCl, $IdHab, $FecIn, $FecFn, $Monto, $FecRes, $IdEst)
	{
		$db=Db::getConnect();
		$update=$db->prepare('	UPDATE reservacion 
								SET IdUsuario=:IdUsuario,
									IdCliente=:IdCliente,
									IdHabitacion=:IdHabitacion,
									FecInicio=:FecInicio,
									FecFin=:FecFin,
                                    Monto=:Monto,
                                    FecReservacion=:FecReservacion,
                                    IdEstatusReservacion=:IdEstatusReservacion
								WHERE IdReservacion=:IdReservacion');
		$update->bindValue(':IdReservacion',$IdRes);
		$update->bindValue(':IdUsuario',$IdUsr);
		$update->bindValue(':IdCliente',$IdCl);
		$update->bindValue(':IdHabitacion',$IdHab);
		$update->bindValue(':IdEstatusReservacion',$IdEst);
		$update->bindValue(':FecInicio',$FecIn);
		$update->bindValue(':FecFin',$FecFn);
        $update->bindValue(':Monto',$Monto);
        $update->bindValue(':FecReservacion',$FecRes);
		$update->execute();	
    }
    
    //la función para crear
	public static function create($IdUsr, $IdCl, $IdHab, $FecIn, $FecFn, $Monto, $FecRes, $IdEst)
	{
		$db=Db::getConnect();
		$insert=$db->prepare('	INSERT INTO reservacion 
								VALUES( NULL,
                                        :IdUsuario,
									    :IdCliente,
									    :IdHabitacion,
									    :FecInicio,
									    :FecFin,
                                        :Monto,
                                        :FecReservacion,
                                        :IdEstatusReservacion
                                        )');
		$insert->bindValue(':IdUsuario',$IdUsr);
		$insert->bindValue(':IdCliente',$IdCl);
		$insert->bindValue(':IdHabitacion',$IdHab);
		$insert->bindValue(':IdEstatusReservacion',$IdEst);
		$insert->bindValue(':FecInicio',$FecIn);
		$insert->bindValue(':FecFin',$FecFn);
        $insert->bindValue(':Monto',$Monto);
        $insert->bindValue(':FecReservacion',$FecRes);
		$insert->execute();	

	}
	
	// la función para eliminar por el id
	public static function delete($id)
	{
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE FROM reservacion WHERE IdReservacion=:IdReservacion');
		$delete->bindValue(':IdReservacion',$id);
		
		$delete->execute();

		return;

	}
}
?>