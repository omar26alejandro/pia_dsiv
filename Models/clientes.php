<?php

class clientes
{
    //función para obtener todos los clientes
    public static function all()
    {
        $db=Db::getConnect();
        $sql=$db->query('SELECT * FROM cliente');
        $Clientes = $sql->fetchAll();
	
		return $Clientes;
	}
	
	public static function Cliente_Usuario()
	{
		$db=Db::getConnect();
        $sql=$db->query('SELECT cliente.IdCliente,
								cliente.Nombre as "CNombre",
								cliente.APaterno as "CAPaterno",
								cliente.AMaterno as "CAMaterno",
								cliente.FecRegistro,
								usuario.Nombre as "UNombre",
								usuario.APaterno as "UAPaterno",
								usuario.AMaterno as "UAMaterno"
						FROM `cliente`
						INNER JOIN usuario
						ON usuario.IdUsuario = cliente.IdUsuario');
        $Clientes = $sql->fetchAll();
	
		return $Clientes;
	}

    // la función para buscar por id
	public static function SelectId($id)
	{
		$db=Db::getConnect();
        $select=$db->prepare('SELECT cliente.IdCliente,
								cliente.Nombre as "CNombre",
								cliente.APaterno as "CAPaterno",
								cliente.AMaterno as "CAMaterno",
								cliente.FecRegistro,
								usuario.Nombre as "UNombre",
								usuario.APaterno as "UAPaterno",
								usuario.AMaterno as "UAMaterno"
						FROM `cliente`
						INNER JOIN usuario
						ON usuario.IdUsuario = cliente.IdUsuario
						WHERE IdCliente=:IdCliente');

		$select->bindValue(':IdCliente',$id);
		$select->execute();

        $Clientes = $select->fetch();
	
		return $Clientes;
  
	}
    
    
	// la función para eliminar por el id
	public static function delete($id)
	{
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE FROM cliente WHERE IdCliente=:IdCliente');
		$delete->bindValue(':IdCliente',$id);

		$delete->execute();

		return;
	}

    
	//funcion para crear
	public static function create($Nom, $APat, $AMat, $IdUsario, $FechRes)
	{
		$db=Db::getConnect();
		$insert=$db->prepare('INSERT INTO cliente VALUES(NUll, :Nombre, :APaterno, :AMaterno, :IdUsuario, :FechRegistro)' );
		$insert->bindValue(':Nombre',$Nom);
		$insert->bindValue(':APaterno',$APat);
		$insert->bindValue(':AMaterno',$AMat);
		$insert->bindValue(':IdUsuario',$IdUsario);
		$insert->bindValue(':FechRegistro',$FechRes);

		$insert ->execute();
        
		return;
    }
    
    	//la funcion de actualizar
	public static function update($IdCliente, $Nombre, $APatm, $AMat)
	{
		$db=Db::getConnect();
		$update=$db->prepare('UPDATE cliente SET Nombre=:Nombre, APaterno=:APaterno, AMaterno=:AMaterno WHERE IdCliente=:IdCliente');
		$update->bindValue(':IdCliente',$IdCliente);
		$update->bindValue(':Nombre',$Nombre);
		$update->bindValue(':APaterno',$APatm);
		$update->bindValue(':AMaterno',$AMat);
		$update->execute();
		return;
	}
}
?>