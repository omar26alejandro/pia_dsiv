<?php

class rol
{
    //función para obtener todos los roles
    public static function all()
    {
        $db=Db::getConnect();
        $sql=$db->query('SELECT * FROM rol');
        $roles = $sql->fetchAll();
	
		return $roles;
    }

    //cargar acceso a pantalla
    public static function pantallas($Rol,$pantalla)
    {
        //buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM rol_pantalla WHERE IdRol=:IdRol AND IdPantalla=:IdPantalla ');
        $select->bindValue(':IdRol',$Rol);
        $select->bindValue(':IdPantalla',$pantalla);
		$select->execute();

		//asignarlo al objeto usuario
		$rolPantalla=$select->fetch();
		return $rolPantalla;
    }

    public static function URLPantalla($id)
    {
        //buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT url FROM pantalla WHERE IdPantalla=:IdPantalla');
        $select->bindValue(':IdPantalla',$id);
		$select->execute();

		//asignarlo al objeto usuario
        $URL=$select->fetch();
        $URL=$URL['url'];
		return $URL;
    }
}
?>