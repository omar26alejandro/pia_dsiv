<?php 

    //Se añade el archivo de modelo de usuario
    require_once(__DIR__ .'/../Models/usuario.php');

    //se añade el archivo para la conexion a la bd
    require_once(__DIR__ .'/../config/connection.php');

    //Se crea la clase del controlador para la funcion de login
	class LoginController
	{	
        public function Login($User,$Pass)
        {          
            //Se manda a llamar a la clase de usuario, del modelo usuario
            $Usr = new Usuario();

            //Se manda a llamar la funcion getById, la cual esta en la clase que llamamos anteriormente
            $data = $Usr->getById($User,$Pass);

            //Se valida si existe el usuario
            if($data!=FALSE)
            {
                //Se crea la sesion
                session_start();
                $_SESSION["usuario"] = $data;

                //Se hace cambio de pagina
                header('Location: ../Views/Inicio.php');
            }
            else
            {
                header('Location: /PIA_DS4');
            }
        }
    }
    
    //Se manada a llamar a la clase que creamos anteriormente
    $LoginController = new LoginController();

    //Se valida que se haya hecho clic en el boton de login
    if(isset($_POST['btnIngresar']))
    {
        //Se manda a llamar a la funcion de login que esta en la clase LoginController
        $LoginController->Login($_POST['Usuario'],$_POST['Pass']);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hotel</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/slider.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/jqtransform.css">
    <script src="js/jquery-1.7.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/cufon-yui.js"></script>
    <script src="js/vegur_400.font.js"></script>
    <script src="js/Vegur_bold_700.font.js"></script>
    <script src="js/cufon-replace.js"></script>
    <script src="js/tms-0.4.x.js"></script>
    <script src="js/jquery.jqtransform.js"></script>
    <script src="js/FF-cash.js"></script>
    <script>
    $(document)
        .ready(function () {
        $('.form-1')
            .jqTransform();
        $('.slider')
            ._TMS({
            show: 0,
            pauseOnHover: true,
            prevBu: '.prev',
            nextBu: '.next',
            playBu: false,
            duration: 1000,
            preset: 'fade',
            pagination: true,
            pagNums: false,
            slideshow: 7000,
            numStatus: false,
            banners: false,
            waitBannerAnimation: false,
            progressBar: false
        })
    });
    </script>
</head>
<body>
    <div class="main" style="background-color:#272727">
        <!--==============================header=================================-->
        <header>
            <div>
            <h1><a href="#"><img src="images/logo.jpg" alt=""></a></h1>
            </div>
        </header>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <!--==============================content================================-->

        <!-- Se crea el form que hara el metodo post para poder hacer el login -->
        <form action="Views/Login.php" method="POST">

            <div style = "margin-top: 5%; margin-left: 40%; margin-bottom: 20%;">
                <table>
                    <tbody>
                        <tr>
                            <td scope="row"> Usuario </td>
                            <td> 
                                <input class="form-control" name="Usuario" type="text"></input>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td scope="row"> Password   </td>
                            <td> 
                                <input class="form-control" name="Pass" type="password"></input>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="padding-left: 22%">
                                <button name="btnIngresar" type="submit" value="Login"> Ingresar</button>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
        <br>
    </div>

    <!--==============================footer=================================-->

    <footer>
        <p>© 2020 Omar Alejandro Leal Marroquin</p>
    </footer>
    <script>Cufon.now();</script>

    <!--==============================footer=================================-->

    <script>Cufon.now();</script>

</body>
</html>
