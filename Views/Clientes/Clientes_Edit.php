<?php
session_start();

  if(!isset($_SESSION["usuario"]))
  {
    header('Location: /PIA_DS4');
  }
  else
  {
    // se añade el archivo de modelos de usuarios
    require_once (__DIR__.'/../../Models/clientes.php');
    require_once (__DIR__.'/../../Models/rol.php');

    //se añade la conexion de bd
    require_once (__DIR__.'/../../config/connection.php');
    
    $rol_pantalla = new rol();

    if($rol_pantalla->pantallas($_SESSION["usuario"]['IdRol'],"3")!=FALSE)
    {
      //Se crea la clase del controlador para la funciones de carga y actualizar
      class ClientesController
      {

        //Carga de cliente
        public function Index($id)
        {
          //Se manda llamar a la funcion all de la clase de clientes
            $Clientes= new clientes();
            $Clientes= $Clientes->SelectId($id);

            //Se retorna todos los registros obtenidos de clientes
            return $Clientes;
        }

            //Funcion para cargar los usuario
            public function cargarUsuarios()
            {
                //Se manda a llamar a la clase de usuario y hace la carga de tipos de usuarios
                $Usuarios = new usuario();
                return $Usuarios->all();
            }

        //Se crea la funcion de actualizar
        public function Update ($IdCliente, $Nombre, $APatm, $AMat)
        {
          //Se manda a llamar a la funcion update de la clase
          $Clientes= new clientes();
          $Clientes->update($IdCliente, $Nombre, $APatm, $AMat);

          //Se hace el cambio de vista despues de hacer el update de CLIENTES
          header('Location: ../Clientes/Clientes_index.php');
        }
      }

      //Se manada a llamar al clase que creamos anteriormente
      $ClientesController= new ClientesController();

      //Se obtiene la url para obtener el parametro enviado
        $url_components = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"); 
        parse_str($url_components['query'], $params); 

        //Se manda a llamar la funcion index de la clase ClientesController
        $Cliente=$ClientesController->Index($params['Cliente']);

        if(isset($_POST['btnAceptar']))
        {
          //Se obtiene la url para obtener el parametro enviado
            $url_components = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"); 
            parse_str($url_components['query'], $params); 

            //Se manda a llamar la funcion index de la clase ClientesController
              $ClientesController->Update($params['Cliente'],$_POST['Nombre'],$_POST['APaterno'], $_POST['AMaterno']);
        }
    }
    else
    {
      header('Location: /Views/Inicio.php');
    }
  }
?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Hotel</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/grid_12.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/slider.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/jqtransform.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
<script src="../../js/jquery-1.7.min.js"></script>
<script src="../../js/jquery.easing.1.3.js"></script>
<script src="../../js/cufon-yui.js"></script>
<script src="../../js/vegur_400.font.js"></script>
<script src="../../js/Vegur_bold_700.font.js"></script>
<script src="../../js/cufon-replace.js"></script>
<script src="../../js/tms-0.4.x.js"></script>
<script src="../../js/jquery.jqtransform.js"></script>
<script src="../../js/FF-cash.js"></script>
<script>
$(document)
    .ready(function () {
    $('.form-1')
        .jqTransform();
    $('.slider')
        ._TMS({
        show: 0,
        pauseOnHover: true,
        prevBu: '.prev',
        nextBu: '.next',
        playBu: false,
        duration: 1000,
        preset: 'fade',
        pagination: true,
        pagNums: false,
        slideshow: 7000,
        numStatus: false,
        banners: false,
        waitBannerAnimation: false,
        progressBar: false
    })
});
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <!--==============================header=================================-->
  <header>
    <div>
      <h1><a href="index.html"><img src="../../images/logo.jpg" alt=""></a></h1>
      <div id="slide">
        <div class="slider">
          <ul class="items">
            <li><img src="../../images/slider-1.jpg" alt=""></li>
            <li><img src="../../images/slider-2.jpg" alt=""></li>
            <li><img src="../../images/slider-3.jpg" alt=""></li>
          </ul>
        </div>
        <a href="#" class="prev"></a><a href="#" class="next"></a> </div>
      <nav>
      <?php if ( $_SESSION["usuario"]["IdRol"] == "1" ) {    ?>
          <ul class="menu">
            <li class="current"><a href="../Inicio.php">Inicio</a></li>
            <li><a href="../Edificios/Edificios_index.php">Edificios</a></li>
            <li><a href="../Habitaciones/Habitacion_index.php">Habitaciones</a></li>
            <li><a href="../Reservaciones/Reservacion_index.php">Reservaciones</a></li>
            <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
            <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
          </ul>
        <?php }
        elseif( $_SESSION["usuario"]["IdRol"] == "2" ){?>
              <ul class="menu">
              <li class="current"><a href="../Inicio.php">Inicio</a></li>
              <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
              <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
            </ul>
        <?php }
        else{?>
              <?php }?>
      </nav>
    </div>
  </header>
  <!--==============================content================================-->
  <br>
  <br>
  <br>
  <br>
      
    <!-- Se crea el form que hara el metodo post para poder hacer el update del edificio -->
    <form action="Clientes_Edit.php?Cliente=<?= $Cliente['IdCliente'] ?>"  method="post">
            <section id="content">
                <div class="row" >
                    <div class="col-sm-2"><label>ID Cliente</label></div>
                    <div class="col-sm-2"><input class="form-control"  minlength="1" maxlength="45" value="<?= $Cliente['IdCliente'] ?>" readonly></input></div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Nombres(s)</label></div>
                    <div class="col-sm-2"><input class="form-control"  minlength="1" maxlength="45" name="Nombre" value="<?= $Cliente['CNombre'] ?>"></input></div>
                </div>
                <br>
				<div class="row" >
                    <div class="col-sm-2"><label>Apellido Paterno</label></div>
                    <div class="col-sm-2"><input class="form-control"  minlength="1" maxlength="45" name="APaterno" value="<?= $Cliente['CAPaterno'] ?>"></input></div>
                </div>
                <br>
				<div class="row" >
                    <div class="col-sm-2"><label>Apellido Materno</label></div>
                    <div class="col-sm-2"><input class="form-control"  minlength="1" maxlength="45" name="AMaterno" value="<?= $Cliente['CAMaterno'] ?>"></input></div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"></div>
                    <div class="col-sm-2"><input type="submit" value="Aceptar" name="btnAceptar"  class="btn btn-primary"></input></div>
                </div>
            </section>
        </form>
</div>
<!--==============================footer=================================-->
<footer>
  <p>© 2020 Omar Alejandro Leal Marroquin</p>
</footer>
<script>Cufon.now();</script>
</body>
</html>
