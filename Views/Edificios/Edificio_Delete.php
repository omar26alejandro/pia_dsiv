<?php 
session_start();

if(!isset($_SESSION["usuario"]))
{
    header('Location: /PIA_DS4');
}
else
{
    
    //Se añade el archivo de modelo de edificios
    require_once(__DIR__ .'/../../Models/edificios.php');
    require_once (__DIR__.'/../../Models/rol.php');

    //se añade la conexion de bd
    require_once (__DIR__.'/../../config/connection.php');
    
    $rol_pantalla = new rol();

    if($rol_pantalla->pantallas($_SESSION["usuario"]['IdRol'],"7")!=FALSE)
    {
        //Se crea la clase del controlador para la funcion de eliminar
        class EdificiosController
        {	
            //Se crea la funcion para eliminar el edificio
            public function delete($id)
            {
                //Se manda a llamar a la funcion delete de la clase edificios
                $Edificios = new edificios();
                $Edificios->delete($id);
                
                //Se hace el cambio de vista despues de hacer el delete de edificios
                header('Location: ../Edificios/Edificios_index.php');
            }
        }

        //Se manada a llamar al clase que creamos anteriormente
        $EdificiosController = new EdificiosController();

        //Se obtiene la url para obtener el parametro enviado
        $url_components = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"); 
        parse_str($url_components['query'], $params); 

        //Se manda a llamar la funcion delete de la clase EdificiosController
        $Edificio=$EdificiosController->delete($params['Edificio']);    
    }
    else
    {
      header('Location: /Views/Inicio.php');
    }
}
?>