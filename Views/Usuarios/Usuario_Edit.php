<?php 
session_start();

	if(!isset($_SESSION["usuario"]))
	{
		header('Location: /PIA_DS4');
	}
	else
	{
        //Se añade el archivo de modelo de rol
        require_once(__DIR__ .'/../../Models/rol.php');
        require_once(__DIR__ .'/../../Models/usuario.php');
        require_once (__DIR__.'/../../Models/rol.php');

        //se añade la conexion de bd
        require_once (__DIR__.'/../../config/connection.php');
        
        $rol_pantalla = new rol();
    
        if($rol_pantalla->pantallas($_SESSION["usuario"]['IdRol'],"19")!=FALSE)
        {
          //Se crea la clase del controlador para la funciones de carga y actualizacion
          class UsuarioController
          {	
              //Se cre la funcion de carga
              public function index($id)
              {
                  //Se manda a llamar a la funcion SelectId de la clase Usuario
                  $Usuario = new usuario();
                  $Usuario = $Usuario->SelectId($id);

                  return $Usuario;
              }

              //Funcion para cargar Roles
              public function cargarRol()
              {
                  //Se manda a llamar a la funcion all de la clase Rol
                  $Rol = new rol();
                  $Rol = $Rol->all();

                  //Se retorna todos los registros obtenidos de habitaciones
                  return $Rol;
              }

              //Se crea la funcion de actualizar
              public function Update($IdUsuario, $Nombre, $APaterno, $AMaterno, $IdRol)
              {
                  //Se manda a llamar a la funcion update de la clase usuario
                  $Usuario = new usuario();
                  $Usuario->update($IdUsuario, $Nombre, $APaterno, $AMaterno, $IdRol);

                  //Se hace el cambio de vista despues de hacer el update de habitaciones
                  header('Location: ../Usuarios/Usuario_index.php');

              }
          }

          //Se manada a llamar al clase que creamos anteriormente
          $UsuarioController = new UsuarioController();

          //Se obtiene la url para obtener el parametro enviado
          $url_components = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"); 
          parse_str($url_components['query'], $params); 

          //Se manda a llamar la funcion index de la clase ReservacionesController
          $Usuario=$UsuarioController->index($params['Usuario']);

          //Se manada a llamar a las funciones de carga de la clase UsuarioController
          $roles = $UsuarioController->cargarRol();

          if(isset($_POST['btnAceptar']))
          {
              //Se obtiene la url para obtener el parametro enviado
              $url_components = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"); 
              parse_str($url_components['query'], $params); 

              $UsuarioController->Update($params['Usuario'],$_POST['Nombre'],$_POST['APaterno'],$_POST['AMaterno'],$_POST['Rol']);
          }
        }
        else
        {
          header('Location: ../Inicio.php');
        }
	}
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Hotel</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/grid_12.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/slider.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/jqtransform.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
<script src="../../js/jquery-1.7.min.js"></script>
<script src="../../js/jquery.easing.1.3.js"></script>
<script src="../../js/cufon-yui.js"></script>
<script src="../../js/vegur_400.font.js"></script>
<script src="../../js/Vegur_bold_700.font.js"></script>
<script src="../../js/cufon-replace.js"></script>
<script src="../../js/tms-0.4.x.js"></script>
<script src="../../js/jquery.jqtransform.js"></script>
<script src="../../js/FF-cash.js"></script>
<script>
$(document)
    .ready(function () {
    $('.form-1')
        .jqTransform();
    $('.slider')
        ._TMS({
        show: 0,
        pauseOnHover: true,
        prevBu: '.prev',
        nextBu: '.next',
        playBu: false,
        duration: 1000,
        preset: 'fade',
        pagination: true,
        pagNums: false,
        slideshow: 7000,
        numStatus: false,
        banners: false,
        waitBannerAnimation: false,
        progressBar: false
    })
});
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <!--==============================header=================================-->
  <header>
    <div>
      <h1><a href="index.html"><img src="../../images/logo.jpg" alt=""></a></h1>
      <div id="slide">
        <div class="slider">
          <ul class="items">
            <li><img src="../../images/slider-1.jpg" alt=""></li>
            <li><img src="../../images/slider-2.jpg" alt=""></li>
            <li><img src="../../images/slider-3.jpg" alt=""></li>
          </ul>
        </div>
        <a href="#" class="prev"></a><a href="#" class="next"></a> </div>
      <nav>
      <?php if ( $_SESSION["usuario"]["IdRol"] == "1" ) {    ?>
          <ul class="menu">
            <li class="current"><a href="../Inicio.php">Inicio</a></li>
            <li><a href="../Edificios/Edificios_index.php">Edificios</a></li>
            <li><a href="../Habitaciones/Habitacion_index.php">Habitaciones</a></li>
            <li><a href="../Reservaciones/Reservacion_index.php">Reservaciones</a></li>
            <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
            <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
          </ul>
        <?php }
        elseif( $_SESSION["usuario"]["IdRol"] == "2" ){?>
              <ul class="menu">
              <li class="current"><a href="../Inicio.php">Inicio</a></li>
              <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
              <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
            </ul>
        <?php }
        else{?>
              <?php }?>
      </nav>
    </div>
  </header>
  <!--==============================content================================-->
  <br>
  <br>
  <br>
  <br>
      
    <!-- Se crea el form que hara el metodo post para poder hacer el update del edificio -->
    <form action="Usuario_Edit.php?Usuario=<?= $Usuario['IdUsuario'] ?>"  method="post">
            <section id="content">
                <div class="row" >
                    <div class="col-sm-2"><label>ID Cliente</label></div>
                    <div class="col-sm-2"><input class="form-control" value="<?= $Usuario['IdUsuario'] ?>" readonly></input></div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Nombres(s)</label></div>
                    <div class="col-sm-2"><input class="form-control" name="Nombre"  minlength="1" maxlength="45" value="<?= $Usuario['Nombre'] ?>"></input></div>
                </div>
                <br>
				<div class="row" >
                    <div class="col-sm-2"><label>Apellido Paterno</label></div>
                    <div class="col-sm-2"><input class="form-control" name="APaterno"  minlength="1" maxlength="45" value="<?= $Usuario['APaterno'] ?>"></input></div>
                </div>
                <br>
				<div class="row" >
                    <div class="col-sm-2"><label>Apellido Materno</label></div>
                    <div class="col-sm-2"><input class="form-control" name="AMaterno"  minlength="1" maxlength="45" value="<?= $Usuario['AMaterno'] ?>"></input></div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Rol: </label></div>
                    <div class="col-sm-4">
                        <select class="form-control" name="Rol" id="Rol">
                        <option value="<?= $Usuario['IdRol'] ?>"><?= $Usuario['Descrip'] ?></option>
                        <?php foreach($roles as $rol):  ?>      
                            <option value="<?= $rol['IdRol'] ?>"><?= $rol['Descrip']?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div> 
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"></div>
                    <div class="col-sm-2"><input type="submit" value="Aceptar" name="btnAceptar"  class="btn btn-primary"></input></div>
                </div>
            </section>
        </form>
</div>
<!--==============================footer=================================-->
<footer>
  <p>© 2020 Omar Alejandro Leal Marroquin</p>
</footer>
<script>Cufon.now();</script>
</body>
</html>
