<?php 
session_start();

if(!isset($_SESSION["usuario"]))
{
    header('Location: /PIA_DS4');
}
else
{
        //Se añade el archivo de modelo de usuario
        require_once(__DIR__ .'/../../Models/rol.php');
        require_once(__DIR__ .'/../../Models/usuario.php');
        require_once (__DIR__.'/../../Models/rol.php');

        //se añade la conexion de bd
        require_once (__DIR__.'/../../config/connection.php');
        
        $rol_pantalla = new rol();
    
        if($rol_pantalla->pantallas($_SESSION["usuario"]['IdRol'],"20")!=FALSE)
        {    
            //Se crea la clase del controlador para la funcion de eliminar
            class UsuarioController
            {	
                //Se crea la funcion para eliminar 
                public function delete($id)
                {
                    //Se manda a llamar a la funcion delete de la clase Usuario
                    $Usuario = new usuario();
                    $Usuario->delete($id);
                    
                    //Se hace el cambio despues del delete 
                    header('Location: ../Usuarios/Usuario_Index.php');
                }
            }
        
            //Se manada a llamar al clase que creamos anteriormente
            $UsuarioController = new UsuarioController();
        
            //Se obtiene la url para obtener el parametro enviado
            $url_components = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"); 
            parse_str($url_components['query'], $params); 
        
        
            //Se manda a llamar la funcion delete de la clase Usuario
            $UsuarioController->delete($params['Usuario']);
        }
        else
        {
          header('Location: ../Inicio.php');
        }
}
?>