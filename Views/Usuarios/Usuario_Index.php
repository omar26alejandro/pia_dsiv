<?php 
session_start();

if(!isset($_SESSION["usuario"]))
{
  header('Location: /PIA_DS4');
}
else
{
      //Se añade el archivo de modelo de Usuario
      require_once(__DIR__ .'/../../Models/usuario.php');
      require_once (__DIR__.'/../../Models/rol.php');

      //se añade la conexion de bd
      require_once (__DIR__.'/../../config/connection.php');
      
      $rol_pantalla = new rol();
  
      if($rol_pantalla->pantallas($_SESSION["usuario"]['IdRol'],"17")!=FALSE)
      {
        //Se crea la clase del controlador para la funcion de carga
        class UsuarioController
        {	
            //Se crea funcion de carga
            public function index()
            {
                //Se manda a llamar a la funcion all de la clase Usuario
                $Usuarios = new usuario();
                $Usuarios = $Usuarios->allUsuarios();
    
                //Se retorna todos los registros obtenidos de edificios
                return $Usuarios;
            }
        }
    
        //Se manada a llamar la funcion index de la clase que creamos anteriormente
        $UsuarioController = new UsuarioController();
        $Usuarios=$UsuarioController->index();  
      }
      else
      {
        header('Location: /Views/Inicio.php');
      }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Hotel</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/grid_12.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/slider.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/jqtransform.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
<script src="../../js/jquery-1.7.min.js"></script>
<script src="../../js/jquery.easing.1.3.js"></script>
<script src="../../js/cufon-yui.js"></script>
<script src="../../js/vegur_400.font.js"></script>
<script src="../../js/Vegur_bold_700.font.js"></script>
<script src="../../js/cufon-replace.js"></script>
<script src="../../js/tms-0.4.x.js"></script>
<script src="../../js/jquery.jqtransform.js"></script>
<script src="../../js/FF-cash.js"></script>
<script>
$(document)
    .ready(function () {
    $('.form-1')
        .jqTransform();
    $('.slider')
        ._TMS({
        show: 0,
        pauseOnHover: true,
        prevBu: '.prev',
        nextBu: '.next',
        playBu: false,
        duration: 1000,
        preset: 'fade',
        pagination: true,
        pagNums: false,
        slideshow: 7000,
        numStatus: false,
        banners: false,
        waitBannerAnimation: false,
        progressBar: false
    })
});
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <!--==============================header=================================-->
  <header>
    <div>
      <h1><a href="index.html"><img src="../../images/logo.jpg" alt=""></a></h1>
      <div id="slide">
        <div class="slider">
          <ul class="items">
            <li><img src="../../images/slider-1.jpg" alt=""></li>
            <li><img src="../../images/slider-2.jpg" alt=""></li>
            <li><img src="../../images/slider-3.jpg" alt=""></li>
          </ul>
        </div>
        <a href="#" class="prev"></a><a href="#" class="next"></a> </div>
      <nav>
      <?php if ( $_SESSION["usuario"]["IdRol"] == "1" ) {    ?>
          <ul class="menu">
            <li class="current"><a href="../Inicio.php">Inicio</a></li>
            <li><a href="../Edificios/Edificios_index.php">Edificios</a></li>
            <li><a href="../Habitaciones/Habitacion_index.php">Habitaciones</a></li>
            <li><a href="../Reservaciones/Reservacion_index.php">Reservaciones</a></li>
            <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
            <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
          </ul>
        <?php }
        elseif( $_SESSION["usuario"]["IdRol"] == "2" ){?>
              <ul class="menu">
              <li class="current"><a href="../Inicio.php">Inicio</a></li>
              <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
              <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
            </ul>
        <?php }
        else{?>
              <?php }?>
      </nav>
    </div>
  </header>
  <!--==============================content================================-->
  <br>
  <br>
  <section id="content">
  <a href="Usuario_Create.php" class="btn btn-primary">Agregar Usuario</a>
  <br>
  <br>
  <div class="row" >
      <div class="col-md-12 table-responsive" >
          <table class="table table-bordered table-hover">
              <thead>
                <tr>
                    <th class="bg-primary" >ID</th>
                    <th class="bg-primary">Nombre(s)</th>
                    <th class="bg-primary">Apellidos</th>
                    <th class="bg-primary">Rol</th>
                    <th class="bg-primary"></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($Usuarios as $Usuario):  ?>    
                <tr>
                  <td class="table-secondary">
                    <?php echo $Usuario['IdUsuario']?>
                  </td>
                  <td  class="table-secondary">
                    <?php echo $Usuario['Nombre']?>
                  </td>
                  <td  class="table-secondary">
                    <?php echo $Usuario['APaterno'] . ' '. $Usuario['AMaterno']?>
                  </td>
                  <td  class="table-secondary">
                    <?php echo $Usuario['Descrip']?>
                  </td>
                  <td class="table-secondary">
                    <a name="edificio" href="Usuario_Edit.php?Usuario=<?= $Usuario['IdUsuario'] ?>" class="btn btn-primary">Editar</a>
                    <a name="edificio" href="Usuario_Delete.php?Usuario=<?= $Usuario['IdUsuario'] ?>" class="btn btn-danger">Eliminar</a>
                  </td>
                </tr> 
                <?php endforeach;  ?>
              </tbody>
          </table>
      </div>
  </div>
  </section>
</div>
<br>
<br>
<!--==============================footer=================================-->
<footer>
  <p>© 2020 Omar Alejandro Leal Marroquin</p>
</footer>
<script>Cufon.now();</script>
</body>
</html>