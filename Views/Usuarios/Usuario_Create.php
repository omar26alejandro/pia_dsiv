<?php 
session_start();

	if(!isset($_SESSION["usuario"]))
	{
		header('Location: /PIA_DS4');
	}
	else
	{
        //Se añade el archivo de modelo de rol
        require_once(__DIR__ .'/../../Models/rol.php');
        require_once(__DIR__ .'/../../Models/usuario.php');
        require_once (__DIR__.'/../../Models/rol.php');

        //se añade la conexion de bd
        require_once (__DIR__.'/../../config/connection.php');
        
        $rol_pantalla = new rol();
    
        if($rol_pantalla->pantallas($_SESSION["usuario"]['IdRol'],"18")!=FALSE)
        {
          //Se crea la clase del controlador para la funciones de carga y actualizacion
          class UsuarioController
          {	
            //Funcion para cargar los tipos de Rol
              public function cargaRol()
              {
                  //Se manda a llamar a la clase de Rol y hace la carga de tipos 
                  $Rol = new rol();
                  return $Rol->all();
              }
      
              //Se crea la funcion de crear
              public function Create($Nombre, $APaterno, $AMaterno, $IdRol)
              {
                  //Se manda a llamar a la funcion create del la clase de Usuario
                  $InsertHab = new usuario();
                  $InsertHab->create($Nombre, $APaterno, $AMaterno, $IdRol);
      
                  //Se hace el cambio de vista despues de hacer el update de Usuario
                  header('Location: ../Usuarios/Usuario_Index.php');
      
              }
          }
      
          //Se manada a llamar al clase que creamos anteriormente
          $UsuarioController = new UsuarioController();
      
          //Se manada a llamar a las funciones de carga de la clase Rol
          $roles = $UsuarioController->cargaRol();
          
      
          if(isset($_POST['btnAceptar']))
          {
              $UsuarioController->Create($_POST['Nombre'],$_POST['APaterno'],$_POST['AMaterno'],$_POST['Rol']);
          }
        }
        else
        {
          header('Location: ../Inicio.php');
        }
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Hotel</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/grid_12.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/slider.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/jqtransform.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
<script src="../../js/jquery-1.7.min.js"></script>
<script src="../../js/jquery.easing.1.3.js"></script>
<script src="../../js/cufon-yui.js"></script>
<script src="../../js/vegur_400.font.js"></script>
<script src="../../js/Vegur_bold_700.font.js"></script>
<script src="../../js/cufon-replace.js"></script>
<script src="../../js/tms-0.4.x.js"></script>
<script src="../../js/jquery.jqtransform.js"></script>
<script src="../../js/FF-cash.js"></script>
<script>
$(document)
    .ready(function () {
    $('.form-1')
        .jqTransform();
    $('.slider')
        ._TMS({
        show: 0,
        pauseOnHover: true,
        prevBu: '.prev',
        nextBu: '.next',
        playBu: false,
        duration: 1000,
        preset: 'fade',
        pagination: true,
        pagNums: false,
        slideshow: 7000,
        numStatus: false,
        banners: false,
        waitBannerAnimation: false,
        progressBar: false
    })
});
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <!--==============================header=================================-->
  <header>
    <div>
      <h1><a href="index.html"><img src="../../images/logo.jpg" alt=""></a></h1>
      <div id="slide">
        <div class="slider">
          <ul class="items">
            <li><img src="../../images/slider-1.jpg" alt=""></li>
            <li><img src="../../images/slider-2.jpg" alt=""></li>
            <li><img src="../../images/slider-3.jpg" alt=""></li>
          </ul>
        </div>
        <a href="#" class="prev"></a><a href="#" class="next"></a> </div>
      <nav>
      <?php if ( $_SESSION["usuario"]["IdRol"] == "1" ) {    ?>
          <ul class="menu">
            <li class="current"><a href="../Inicio.php">Inicio</a></li>
            <li><a href="../Edificios/Edificios_index.php">Edificios</a></li>
            <li><a href="../Habitaciones/Habitacion_index.php">Habitaciones</a></li>
            <li><a href="../Reservaciones/Reservacion_index.php">Reservaciones</a></li>
            <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
            <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
          </ul>
        <?php }
        elseif( $_SESSION["usuario"]["IdRol"] == "2" ){?>
              <ul class="menu">
              <li class="current"><a href="../Inicio.php">Inicio</a></li>
              <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
              <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
            </ul>
        <?php }
        else{?>
              <?php }?>
      </nav>
    </div>
  </header>
  <!--==============================content================================-->
  <br>
  <br>
  <br>
  <br>
      
    <!-- Se crea el form que hara el metodo post para poder hacer el update del edificio -->
    <form action="Usuario_Create.php"  method="post">
            <section id="content">
                <div class="row" >
                    <div class="col-sm-2"><label>Nombres(s)</label></div>
                    <div class="col-sm-2"><input class="form-control"  minlength="1" maxlength="45" name="Nombre" required></input></div>
                </div>
                <br>
				<div class="row" >
                    <div class="col-sm-2"><label>Apellido Paterno</label></div>
                    <div class="col-sm-2"><input class="form-control"  minlength="1" maxlength="45" name="APaterno" required></input></div>
                </div>
                <br>
				<div class="row" >
                    <div class="col-sm-2"><label>Apellido Materno</label></div>
                    <div class="col-sm-2"><input class="form-control"  minlength="1" maxlength="45" name="AMaterno" required></input></div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Rol: </label></div>
                    <div class="col-sm-4">
                        <select class="form-control" name="Rol" id="Rol" required>
                        <?php foreach($roles as $rol):  ?>      
                            <option value="<?= $rol['IdRol'] ?>"><?= $rol['Descrip']?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div> 
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"></div>
                    <div class="col-sm-2"><input type="submit" value="Aceptar" name="btnAceptar"  class="btn btn-primary"></input></div>
                </div>
            </section>
        </form>
</div>
<!--==============================footer=================================-->
<footer>
  <p>© 2020 Omar Alejandro Leal Marroquin</p>
</footer>
<script>Cufon.now();</script>
</body>
</html>
