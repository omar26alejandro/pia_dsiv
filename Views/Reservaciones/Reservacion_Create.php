<?php 
session_start();

	if(!isset($_SESSION["usuario"]))
	{
		header('Location: /PIA_DS4');
	}
	else
	{
        //Se añade el archivo de modelo de reservaciones, habitaciones, usuario y cliente
        require_once(__DIR__ .'/../../Models/reservaciones.php');
        require_once(__DIR__ .'/../../Models/habitaciones.php');
        require_once(__DIR__ .'/../../Models/usuario.php');
        require_once(__DIR__ .'/../../Models/clientes.php');
        require_once (__DIR__.'/../../Models/rol.php');

        //se añade la conexion de bd
        require_once (__DIR__.'/../../config/connection.php');
        
        $rol_pantalla = new rol();
    
        if($rol_pantalla->pantallas($_SESSION["usuario"]['IdRol'],"13")!=FALSE)
        {
            //Se crea la clase del controlador para la funciones de carga e inserccion de los datos
            class ReservacionesController
            {	
                //Funcion para cargar las habitaciones
                public function cargaHabitaciones()
                {
                    //Se manda a llamar a la funcion all de la clase habitaciones
                    $Habitaciones = new habitaciones();
                    $Habitaciones= $Habitaciones->all();

                    //Se retorna todos los registros obtenidos de habitaciones
                    return $Habitaciones;
                }

                //Funcion para cargar los usuario
                public function cargarUsuarios()
                {
                    //Se manda a llamar a la clase de usuario y hace la carga de tipos de usuarios
                    $Usuarios = new usuario();
                    return $Usuarios->all();
                }

                //Funcion para cargar de estatus de reservacion
                public function cargaEstatusReservacion()
                {
                    //Se manda a llamar a la clase de reservaciones y hace la carga de tipos de estatus de reservacion
                    $Tipos = new reservaciones();
                    return $Tipos->SelectEstus();
                }

                //Funcion para cargar clientes
                public function cargaCliente()
                {
                    //Se manda a llamar a la clase clientes y se hace la carga de los clientes
                    $Clientes = new clientes();
                    return $Clientes->all();
                }
                
                //Se crea la funcion de actualizar
                public function Create($IdUsr, $IdCl, $IdHab, $FecIn, $FecFn, $Monto, $FecRes, $IdEst)
                {
                    //Se manda a llamar a la funcion update de la clase habitaciones
                    $Reservacion = new reservaciones();
                    $Reservacion->create($IdUsr, $IdCl, $IdHab, $FecIn, $FecFn, $Monto, $FecRes, $IdEst);

                    //Se hace el cambio de vista despues de hacer el update de habitaciones
                    header('Location: ../Reservaciones/Reservacion_index.php');

                }
            }

            //Se manada a llamar al clase que creamos anteriormente
            $ReservacionesController = new ReservacionesController();

            //Se manada a llamar a las funciones de carga de la clase HabitacionesController
            $habitaciones = $ReservacionesController->cargaHabitaciones();
            $estatus = $ReservacionesController->cargaEstatusReservacion();
            $usuarios = $ReservacionesController->cargarUsuarios();
            $clientes = $ReservacionesController->cargaCliente();

            if(isset($_POST['btnAceptar']))
            {
                session_start();
                
                //Se manda a llamar la funcion create de la clase ReservacionesController
                $ReservacionesController->Create( $_SESSION["usuario"]['IdUsuario'], 
                                                $_POST['Cliente'], $_POST['Habitacion'], $_POST['FecInicio'], 
                                                $_POST['FecFin'], $_POST['Monto'], $_POST['FecReser'], $_POST['Estatus']);
            }
        }
        else
        {
          header('Location: ../Inicio.php');
        }

	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Hotel</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/grid_12.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/slider.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/jqtransform.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
<script src="../../js/jquery-1.7.min.js"></script>
<script src="../../js/jquery.easing.1.3.js"></script>
<script src="../../js/cufon-yui.js"></script>
<script src="../../js/vegur_400.font.js"></script>
<script src="../../js/Vegur_bold_700.font.js"></script>
<script src="../../js/cufon-replace.js"></script>
<script src="../../js/tms-0.4.x.js"></script>
<script src="../../js/jquery.jqtransform.js"></script>
<script src="../../js/FF-cash.js"></script>
<script>
$(document)
    .ready(function () {
    $('.form-1')
        .jqTransform();
    $('.slider')
        ._TMS({
        show: 0,
        pauseOnHover: true,
        prevBu: '.prev',
        nextBu: '.next',
        playBu: false,
        duration: 1000,
        preset: 'fade',
        pagination: true,
        pagNums: false,
        slideshow: 7000,
        numStatus: false,
        banners: false,
        waitBannerAnimation: false,
        progressBar: false
    })
});
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <!--==============================header=================================-->
  <header>
    <div>
      <h1><a href="index.html"><img src="../../images/logo.jpg" alt=""></a></h1>
      <div id="slide">
        <div class="slider">
          <ul class="items">
            <li><img src="../../images/slider-1.jpg" alt=""></li>
            <li><img src="../../images/slider-2.jpg" alt=""></li>
            <li><img src="../../images/slider-3.jpg" alt=""></li>
          </ul>
        </div>
        <a href="#" class="prev"></a><a href="#" class="next"></a> </div>
      <nav>
      <?php if ( $_SESSION["usuario"]["IdRol"] == "1" ) {    ?>
          <ul class="menu">
            <li class="current"><a href="../Inicio.php">Inicio</a></li>
            <li><a href="../Edificios/Edificios_index.php">Edificios</a></li>
            <li><a href="../Habitaciones/Habitacion_index.php">Habitaciones</a></li>
            <li><a href="../Reservaciones/Reservacion_index.php">Reservaciones</a></li>
            <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
            <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
          </ul>
        <?php }
        elseif( $_SESSION["usuario"]["IdRol"] == "2" ){?>
              <ul class="menu">
              <li class="current"><a href="../Inicio.php">Inicio</a></li>
              <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
              <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
            </ul>
        <?php }
        else{?>
              <?php }?>
      </nav>
    </div>
  </header>
  <!--==============================content================================-->
  <br>
  <br>
  <br>
  <br>
      
    <!-- Se crea el form que hara el metodo post para poder hacer el create de la reservacion -->
    <form action="Reservacion_Create.php"  method="post">
            <section id="content">
                <div class="row" >
                    <div class="col-sm-2"><label>Cliente: </label></div>
                    <div class="col-sm-4">
                        <select class="form-control" name="Cliente" id="Cliente" required>
                        <?php foreach($clientes as $cliente):  ?>      
                            <option value="<?= $cliente['IdCliente'] ?>"><?= $cliente['Nombre']. ' '.$cliente['APaterno']. ' '.$cliente['AMaterno'] ?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div>                
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Habitacion: </label></div>
                    <div class="col-sm-4">
                        <select class="form-control" name="Habitacion" id="Habitacion" required>
                        <?php foreach($habitaciones as $habitacion):  ?>      
                            <option value="<?= $habitacion['IdHabitacion'] ?>"><?= $habitacion['DescHabitacion']?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div>  
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Fecha Incio: </label></div>
                    <div class="col-sm-2"><input class="form-control" type="Date" name="FecInicio" required></input></div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Fecha Fin: </label></div>
                    <div class="col-sm-2"><input class="form-control" type="Date" name="FecFin" required></input></div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Monto: </label></div>
                    <div class="col-sm-2"><input class="form-control" name="Monto"  minlength="1" maxlength="15" type="number" required></input></div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Fecha Reservacion: </label></div>
                    <div class="col-sm-2"><input class="form-control" type="Date" name="FecReser" required></input></div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Estatus: </label></div>
                    <div class="col-sm-4">
                        <select class="form-control" name="Estatus" id="Estatus" required>
                        <?php foreach($estatus as $estatu):  ?>      
                            <option value="<?= $estatu['IdEstatusReservacion'] ?>"><?= $estatu['DescripEstatusReser']?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div>  
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                    <input type="submit" name="btnAceptar" value="Aceptar" class="btn btn-primary"></input>
                    </div>
                </div>             
                <br>
            </section>
        </form>
</div>
<!--==============================footer=================================-->
<footer>
  <p>© 2020 Omar Alejandro Leal Marroquin</p>
</footer>
<script>Cufon.now();</script>
</body>
</html>
