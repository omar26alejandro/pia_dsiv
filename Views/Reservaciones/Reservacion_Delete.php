<?php 
session_start();

	if(!isset($_SESSION["usuario"]))
	{
		header('Location: /PIA_DS4');
	}
	else
	{
        //Se añade el archivo de modelo de reservaciones
        require_once(__DIR__ .'/../../Models/reservaciones.php');
        require_once (__DIR__.'/../../Models/rol.php');

        //se añade la conexion de bd
        require_once (__DIR__.'/../../config/connection.php');
        
        $rol_pantalla = new rol();
    
        if($rol_pantalla->pantallas($_SESSION["usuario"]['IdRol'],"16")!=FALSE)
        {
            //Se crea la clase del controlador para la funcion de eliminar
            class ReservacionesController
            {	
                //Se crea la funcion para eliminar la reservacion
                public function delete($id)
                {
                    //Se manda a llamar a la funcion delete de la clase reservacion
                    $Reservaciones = new reservaciones();
                    $Reservaciones->delete($id);
                    
                    //Se hace el cambio de vista despues de hacer el delete de reservacion
                    header('Location: ../Reservaciones/Reservacion_index.php');
                }
            }

            //Se manada a llamar al clase que creamos anteriormente
            $ReservacionesController = new ReservacionesController();

            //Se obtiene la url para obtener el parametro enviado
            $url_components = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"); 
            parse_str($url_components['query'], $params); 

            //Se manda a llamar la funcion delete de la clase HabitacionesController
            $ReservacionesController->delete($params['Reservacion']);
        }
        else
        {
          header('Location: ../Inicio.php');
        }
	}
?>