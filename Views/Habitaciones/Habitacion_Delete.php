<?php 
    session_start();

	if(!isset($_SESSION["usuario"]))
	{
		header('Location: /PIA_DS4');
	}
	else
	{
        //Se añade el archivo de modelo de habitaciones
        require_once(__DIR__ .'/../../Models/habitaciones.php');
        require_once (__DIR__.'/../../Models/rol.php');

        //se añade la conexion de bd
        require_once (__DIR__.'/../../config/connection.php');
        
        $rol_pantalla = new rol();
    
        if($rol_pantalla->pantallas($_SESSION["usuario"]['IdRol'],"12")!=FALSE)
        {
            //Se crea la clase del controlador para la funcion de eliminar
            class HabitacionesController
            {	
                //Se crea la funcion para eliminar la habitacion
                public function delete($id)
                {
                    //Se manda a llamar a la funcion delete de la clase habitaciones
                    $Habitaciones = new habitaciones();
                    $Habitaciones->delete($id);
                    
                    //Se hace el cambio de vista despues de hacer el delete de habitaciones
                    header('Location: ../Habitaciones/Habitacion_index.php');
                }
            }

            //Se manada a llamar al clase que creamos anteriormente
            $HabitacionesController = new HabitacionesController();

            //Se obtiene la url para obtener el parametro enviado
            $url_components = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"); 
            parse_str($url_components['query'], $params); 

            //Se manda a llamar la funcion delete de la clase HabitacionesController
            $HabitacionesController->delete($params['Habitacion']);
        }
        else
        {
          header('Location: ../Inicio.php');
        }
	}
?>