<?php 
session_start();

	if(!isset($_SESSION["usuario"]))
	{
		header('Location: /PIA_DS4');
	}
	else
	{
        //Se añade el archivo de modelo de edificios, habitaciones, vista y estatus
        require_once(__DIR__ .'/../../Models/edificios.php');
        require_once(__DIR__ .'/../../Models/habitaciones.php');
        require_once(__DIR__ .'/../../Models/vista.php');
        require_once(__DIR__ .'/../../Models/estatus.php');  
        require_once (__DIR__.'/../../Models/rol.php');

        //se añade la conexion de bd
        require_once (__DIR__.'/../../config/connection.php');
        
        $rol_pantalla = new rol();
    
        if($rol_pantalla->pantallas($_SESSION["usuario"]['IdRol'],"11")!=FALSE)
        {
            //Se crea la clase del controlador para la funciones de carga y actualizacion
            class HabitacionesController
            {	
                //Funcion para cargar los tipos de vistas
                public function cargaVistas()
                {
                    //Se manda a llamar a la clase de vista y hace la carga de tipos de vistas
                    $Vistas = new vista();
                    return $Vistas->all();
                }

                //Funcion para cargar los tipos de estatus
                public function cargaEstatus()
                {
                    //Se manda a llamar a la clase de estatus y hace la carga de tipos de estatus
                    $Estatus = new estatus();
                    return $Estatus->all();
                }

                //Funcion para cargar los edificios
                public function cargaEdificios()
                {
                    //Se manda a llamar a la clase de edificios y hace la carga de tipos de edificios
                    $Edificios = new edificios();
                    return $Edificios->all();
                }

                //Funcion para cargar los tipos de habitaciones
                public function cargaTipoHabitaciones()
                {
                    //Se manda a llamar a la clase de habitaciones y hace la carga de tipos de habitaciones
                    $Habitaciones = new habitaciones();
                    return $Habitaciones->SelectTipoHabitacion();
                }
                
                //Se crea la funcion de crear
                public function Create($idEdif, $Desc, $Nivel, $TipHab, $Vista, $IdUsuario, $Estatus)
                {
                    //Se manda a llamar a la funcion create del la clase de habitaciones
                    $InsertHab = new habitaciones();
                    $InsertHab->create($Desc, $Nivel, $TipHab, $idEdif, $Vista, $IdUsuario, $Estatus);

                    //Se hace el cambio de vista despues de hacer el update de habitaciones
                    header('Location: ../Habitaciones/Habitacion_index.php');

                }
            }

            //Se manada a llamar al clase que creamos anteriormente
            $HabitacionesController = new HabitacionesController();

            //Se manada a llamar a las funciones de carga de la clase HabitacionesController
            $habitaciones = $HabitacionesController->cargaTipoHabitaciones();
            $vistas = $HabitacionesController->cargaVistas();
            $estatus = $HabitacionesController->cargaEstatus();
            $edificios = $HabitacionesController->cargaEdificios();

            if(isset($_POST['btnAceptar']))
            {
                session_start();

                //Se manda a llamar la funcion crear de la clase HabitacionesController
                $HabitacionesController->Create( $_POST['Edificio'], $_POST['Descripcion'], 
                $_POST['Nivel'], $_POST['Tipo'], $_POST['Vista'],  $_SESSION["usuario"]['IdUsuario'], $_POST['Estatus']);
            }
        }
        else
        {
          header('Location: ../Inicio.php');
        }
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Hotel</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/grid_12.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/slider.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../css/jqtransform.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
<script src="../../js/jquery-1.7.min.js"></script>
<script src="../../js/jquery.easing.1.3.js"></script>
<script src="../../js/cufon-yui.js"></script>
<script src="../../js/vegur_400.font.js"></script>
<script src="../../js/Vegur_bold_700.font.js"></script>
<script src="../../js/cufon-replace.js"></script>
<script src="../../js/tms-0.4.x.js"></script>
<script src="../../js/jquery.jqtransform.js"></script>
<script src="../../js/FF-cash.js"></script>
<script>
$(document)
    .ready(function () {
    $('.form-1')
        .jqTransform();
    $('.slider')
        ._TMS({
        show: 0,
        pauseOnHover: true,
        prevBu: '.prev',
        nextBu: '.next',
        playBu: false,
        duration: 1000,
        preset: 'fade',
        pagination: true,
        pagNums: false,
        slideshow: 7000,
        numStatus: false,
        banners: false,
        waitBannerAnimation: false,
        progressBar: false
    })
});
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <!--==============================header=================================-->
  <header>
    <div>
      <h1><a href="index.html"><img src="../../images/logo.jpg" alt=""></a></h1>
      <div id="slide">
        <div class="slider">
          <ul class="items">
            <li><img src="../../images/slider-1.jpg" alt=""></li>
            <li><img src="../../images/slider-2.jpg" alt=""></li>
            <li><img src="../../images/slider-3.jpg" alt=""></li>
          </ul>
        </div>
        <a href="#" class="prev"></a><a href="#" class="next"></a> </div>
      <nav>
      <?php if ( $_SESSION["usuario"]["IdRol"] == "1" ) {    ?>
            <ul class="menu">
              <li class="current"><a href="../Inicio.php">Inicio</a></li>
              <li><a href="../Edificios/Edificios_index.php">Edificios</a></li>
              <li><a href="../Habitaciones/Habitacion_index.php">Habitaciones</a></li>
              <li><a href="../Reservaciones/Reservacion_index.php">Reservaciones</a></li>
              <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
              <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
            </ul>
          <?php }
          elseif( $_SESSION["usuario"]["IdRol"] == "2" ){?>
                <ul class="menu">
                <li class="current"><a href="../Inicio.php">Inicio</a></li>
                <li><a href="../Clientes/Clientes_index.php">Clientes</a></li>
                <li><a href="../Usuarios/Usuario_Index.php">Usuarios</a></li>
              </ul>
          <?php }
          else{?>
            <?php }?>
      </nav>
    </div>
  </header>
  <!--==============================content================================-->
  <br>
  <br>
  <br>
  <br>
      
    <!-- Se crea el form que hara el metodo post para poder hacer el create de la habitacion -->
    <form action="Habitacion_Create.php"  method="post">
            <section id="content">
                <div class="row" >
                    <div class="col-sm-2"><label>Desc. de Habitación: </label></div>
                    <div class="col-sm-2"><input class="form-control"  minlength="1" maxlength="45" name="Descripcion" required></input></div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Nivel: </label></div>
                    <div class="col-sm-2"><input class="form-control"  minlength="1" maxlength="45" name="Nivel" required></input></div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Tipo de Habitacion: </label></div>
                    <div class="col-sm-4">
                        <select class="form-control" name="Tipo" id="Tipo" required>
                            <?php foreach($habitaciones as $habitacion):  ?>      
                              <option value="<?= $habitacion['IdTipoHabitacion'] ?>"><?= $habitacion['DescpTipoHabitacion'] ?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div>
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Edificio: </label></div>
                    <div class="col-sm-4">
                        <select class="form-control" name="Edificio" id="Edificio" required>
                            <?php foreach($edificios as $edificio):  ?>      
                              <option value="<?= $edificio['IdEdificio'] ?>"><?= $edificio['DescEdificio'] ?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div>                
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Vista: </label></div>
                    <div class="col-sm-4">
                        <select class="form-control" name="Vista" id="Vista" required>
                            <?php foreach($vistas as $vista):  ?>      
                              <option value="<?= $vista['IdVista'] ?>"><?= $vista['DescVista'] ?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div>                
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"><label>Estatus: </label></div>
                    <div class="col-sm-4">
                        <select class="form-control" name="Estatus" id="Estatus" required>
                            <?php foreach($estatus as $estatu):  ?>      
                              <option value="<?= $estatu['IdEstatus'] ?>"><?= $estatu['DescEstatus'] ?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div>                
                </div>
                <br>
                <div class="row" >
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                    <input type="submit" name="btnAceptar" value="Aceptar" class="btn btn-primary"></input>
                    </div>
                </div>             
                <br>
            </section>
        </form>
</div>
<!--==============================footer=================================-->
<footer>
  <p>© 2020 Omar Alejandro Leal Marroquin</p>
</footer>
<script>Cufon.now();</script>
</body>
</html>
