<?php session_start(); if (isset($_SESSION["usuario"])) {    ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Hotel</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="../css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/grid_12.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/slider.css">
<link rel="stylesheet" type="text/css" media="screen" href="../css/jqtransform.css">
<script src="../js/jquery-1.7.min.js"></script>
<script src="../js/jquery.easing.1.3.js"></script>
<script src="../js/cufon-yui.js"></script>
<script src="../js/vegur_400.font.js"></script>
<script src="../js/Vegur_bold_700.font.js"></script>
<script src="../js/cufon-replace.js"></script>
<script src="../js/tms-0.4.x.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/FF-cash.js"></script>
<script>
$(document)
    .ready(function () {
    $('.form-1')
        .jqTransform();
    $('.slider')
        ._TMS({
        show: 0,
        pauseOnHover: true,
        prevBu: '.prev',
        nextBu: '.next',
        playBu: false,
        duration: 1000,
        preset: 'fade',
        pagination: true,
        pagNums: false,
        slideshow: 7000,
        numStatus: false,
        banners: false,
        waitBannerAnimation: false,
        progressBar: false
    })
});
</script>

</head>
<body>
<div class="main">
  <!--==============================header=================================-->
  <header>
    <div>
      <h1><a href="index.html"><img src="../images/logo.jpg" alt=""></a></h1>
      <div id="slide">
        <div class="slider">
          <ul class="items">
            <li><img src="../images/slider-1.jpg" alt=""></li>
            <li><img src="../images/slider-2.jpg" alt=""></li>
            <li><img src="../images/slider-3.jpg" alt=""></li>
          </ul>
        </div>
        <a href="#" class="prev"></a><a href="#" class="next"></a> </div>
      <nav>
      <?php if ( $_SESSION["usuario"]["IdRol"] == "1" ) {    ?>
        <ul class="menu">
          <li class="current"><a href="../Views/Inicio.php">Inicio</a></li>
          <li><a href="../Views/Edificios/Edificios_index.php">Edificios</a></li>
          <li><a href="../Views/Habitaciones/Habitacion_index.php">Habitaciones</a></li>
          <li><a href="../Views/Reservaciones/Reservacion_index.php">Reservaciones</a></li>
          <li><a href="../Views/Clientes/Clientes_index.php">Clientes</a></li>
          <li><a href="../Views/Usuarios/Usuario_Index.php">Usuarios</a></li>
        </ul>
      <?php }
      elseif( $_SESSION["usuario"]["IdRol"] == "2" ){?>
            <ul class="menu">
            <li class="current"><a href="../Views/Inicio.php">Inicio</a></li>
            <li><a href="../Views/Clientes/Clientes_index.php">Clientes</a></li>
            <li><a href="../Views/Usuarios/Usuario_Index.php">Usuarios</a></li>
          </ul>
      <?php }
      else{?>
            <?php }?>
      </nav>
    </div>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_8">
        <h2 class="top-1 p3">Welcome message!</h2>
        <p class="p2">Nuestro Hotel cuenta con las mejores instalaciones para que usted y su familia puedan disfrutar de su estancia en esta paradisiaca ciudad; pueden hacer uso de cualquiera de ellas sin ningún costo adicional; por favor comuníquese con nuestra ama de llaves para tener detalles sobre todos los servicios que le ofrecemos.</p>
      </div>
      <div class="clear"></div>
    </div>
  </section>
</div>
<!--==============================footer=================================-->
<footer>
  <p>© 2020 Omar Alejandro Leal Marroquin</p>
</footer>
<script>Cufon.now();</script>
</body>
</html>
<?php }    
else 
{
  header('Location: /PIA_DS4');
}?>